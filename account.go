package lichess

import (
	"encoding/json"
	"log"
)

// Account is an overview of most information avaiable about an account
type Account struct {
	ID                int      `json:"string"`
	Username          string   `json:"username"`
	Title             string   `json:"title"`
	Online            bool     `json:"online"`
	Playing           bool     `json:"playing"`
	Streaming         bool     `json:"streaming"`
	CreatedAt         int      `json:"createdAt"`
	SeenAt            int      `json:"seenAt"`
	Profile           Profile  `json:"profile"`
	NumberOfFollowers int      `json:"nbFollowers"`
	NumberFollowing   int      `json:"nbFollowing"`
	CompletionRate    int      `json:"completionRate"`
	Language          string   `json:"language"`
	Count             Count    `json:"count"`
	Perfs             Perfs    `json:"count"`
	Patron            bool     `json:"patron"`
	Disabled          bool     `json:"disabled"`
	Engine            bool     `json:"engine"`
	Booster           bool     `json:"booster"`
	PlayTime          Playtime `json:"playTime"`
}

// Profile has personal information about the user
type Profile struct {
	Bio       string `json:"bio"`
	Country   string `json:"country"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Links     string `json:"links"`
	Location  string `json:"location"`
}

// Count contains a list of data counters about the user
type Count struct {
	AI       int `json:"ai"`
	All      int `json:"all"`
	Bookmark int `json:"bookmark"`
	Draw     int `json:"draw"`
	DrawH    int `json:"drawH"`
	Import   int `json:"import"`
	Loss     int `json:"loss"`
	LossH    int `json:"lossH"`
	Me       int `json:"me"`
	Playing  int `json:"playing"`
	Rater    int `json:"rated"`
	Win      int `json:"win"`
	WinH     int `json:"winH"`
}

// Perfs contains a list of Perf structs from the user
type Perfs struct {
	Blitz    Perf `json:"blitz"`
	Bullet   Perf `json:"bullet"`
	Chess960 Perf `json:"chess960"`
	Puzzle   Perf `json:"puzzle"`
}

// Perf contains actual perf information
type Perf struct {
	Games  int `json:"games"`
	Prog   int `json:"prog"`
	Rating int `json:"rating"`
	RD     int `json:"rd"`
}

// Playtime has information about the playtime of the user
type Playtime struct {
	Total int `json:"total"`
	TV    int `json:"tv"`
}

// Email is a struct that only contains an email
type Email struct {
	Email string `json:"email"`
}

// Preferences is a struct that contains a list of all the users' preferences
type Preferences struct {
	Dark          bool   `json:"dark"`
	Transp        bool   `json:"transp"`
	BGImg         string `json:"bgImg"`
	Is3D          bool   `json:"is3d"`
	Theme         string `json:"theme"`
	PieceSet      string `json:"pieceSet"`
	Theme3D       string `json:"theme3d"`
	PieceSet3D    string `json:"pieceSet3d"`
	SoundSet      string `json:"soundSet"`
	Blindfold     int    `json:"blindfold"`
	AutoQueen     int    `json:"autoQueen"`
	AutoThreefold int    `json:"autoThreefold"`
	Takeback      int    `json:"takeback"`
	ClockTenths   int    `json:"clockTenths"`
	ClockBar      bool   `json:"clockBar"`
	ClockSound    bool   `json:"clockSound"`
	Premove       bool   `json:"premove"`
	Animation     int    `json:"animation"`
	Captured      bool   `json:"captured"`
	Follow        bool   `json:"follow"`
	Highlight     bool   `json:"highlight"`
	Destination   bool   `json:"destination"`
	Coords        int    `json:"coords"`
	Replay        int    `json:"replay"`
	Challenge     int    `json:"challenge"`
	Message       int    `json:"message"`
	CoordColor    int    `json:"coordColor"`
	SubmitMove    int    `json:"submitMove"`
	ConfirmResign int    `json:"confirmResign"`
	InsightShare  int    `json:"insightShare"`
	KeyboardMove  int    `json:"keyboardMove"`
	Zen           int    `json:"zen"`
	MoveEvent     int    `json:"moveEvent"`
}

// Kid is a struct that contains a boolean stating if the current user is in kid mode
type Kid struct {
	Kid bool `json:"kid"`
}

// GetAccount will return an Account object
func (client *Client) GetAccount() (Account, error) {
	var account Account

	response, err := client.get("account")
	if err != nil {
		log.Fatalln(err)
		return account, err
	}
	json.Unmarshal(response, &account)

	return account, err
}

// GetEmail will return the email from the user
func (client *Client) GetEmail() (string, error) {
	var email Email
	response, err := client.get("account/email")
	if err != nil {
		log.Fatalln(err)
		return "", err
	}
	json.Unmarshal(response, &email)

	return email.Email, err
}

// GetPreferences returns a Preferences struct with all the users' preferences
func (client *Client) GetPreferences() (Preferences, error) {
	var preferences Preferences
	response, err := client.get("account/preferences")
	if err != nil {
		log.Fatalln(err)
		return preferences, err
	}
	json.Unmarshal(response, &preferences)

	return preferences, err
}

// IsKidMode returns a boolean stating if the current user is in kid mode
func (client *Client) IsKidMode() (bool, error) {
	var kid Kid
	response, err := client.get("account/kid")
	if err != nil {
		log.Fatalln(err)
		return false, err
	}
	json.Unmarshal(response, &kid)

	return kid.Kid, err
}
