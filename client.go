package lichess

import (
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

const (
	baseURL = "https://lichess.org/api/"
)

// Client represents a client to talk to the lichess API
type Client struct {
	http *http.Client
	key  string
}

// NewClient creates a new Client with defaults
func NewClient(key string) Client {
	client := http.Client{
		Timeout: time.Duration(time.Second * 10),
	}
	return Client{
		http: &client,
		key:  key,
	}
}

func (client *Client) get(url string) ([]byte, error) {
	fullURL := baseURL + url
	request, err := http.NewRequest("GET", fullURL, nil)
	if err != nil {
		log.Fatalln(err)
		return nil, err
	}

	request.Header.Set("Authorization", "Bearer "+client.key)
	response, err := client.http.Do(request)
	if err != nil {
		log.Fatalln(err)
		return nil, err
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatalln(err)
		return nil, err
	}

	return body, nil
}
